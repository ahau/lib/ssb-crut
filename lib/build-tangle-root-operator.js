const { seekKey } = require('bipf')

const B_VALUE = Buffer.from('value')
const B_CONTENT = Buffer.from('content')
const B_TANGLES = Buffer.from('tangles')
const B_ROOT = Buffer.from('root')

module.exports = function buildTangleRootOperator (tangle) {
  const { equal } = require('ssb-db2/operators')
  const B_TANGLE = Buffer.from(tangle)

  function seekTangleRoot (buffer) {
    let p = 0 // note you pass in p!
    p = seekKey(buffer, p, B_VALUE)
    if (p < 0) return
    p = seekKey(buffer, p, B_CONTENT)
    if (p < 0) return
    p = seekKey(buffer, p, B_TANGLES)
    if (p < 0) return
    p = seekKey(buffer, p, B_TANGLE)
    if (p < 0) return
    return seekKey(buffer, p, B_ROOT)
  }

  return function tangleRoot (value) {
    return equal(seekTangleRoot, value, {
      prefix: 32,
      prefixOffset: 1,
      indexType: `tangle_${tangle}_root`
    })
  }
}
