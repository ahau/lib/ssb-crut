const pull = require('pull-stream')

const { getCanonicalContent } = require('./util')

module.exports = function GetUpdates (ssb, crut) {
  return ssb.db // test is isDB2
    ? GetUpdates2(ssb, crut)
    : GetUpdates1(ssb, crut)
}

function GetUpdates1 (ssb, crut) {
  const { spec: { type, tangle, getTransformation } } = crut
  const isUpdate = msg => crut.isUpdate(getCanonicalContent(msg, getTransformation))

  return function getUpdates (id, cb) {
    const query = [{
      $filter: {
        dest: id,
        value: {
          content: {
            type,
            tangles: {
              [tangle]: { root: id }
            }
          }
        }
      }
    }]

    pull(
      ssb.backlinks.read({ query }),
      pull.filter(isUpdate),
      pull.collect(cb)
    )
  }
}

function GetUpdates2 (ssb, crut) {
  const { spec } = crut
  const tangleRoot = require('./build-tangle-root-operator')(spec.tangle)
  const isUpdate = msg => crut.isUpdate(getCanonicalContent(msg, spec.getTransformation))

  return function getUpdates (id, cb) {
    const { where, and, type, toPullStream } = ssb.db.operators
    pull(
      ssb.db.query(
        where(
          and(
            type(spec.type),
            tangleRoot(id)
          )
        ),
        toPullStream()
      ),
      pull.filter(m => m.value.content.tangles[spec.tangle].root === id),
      pull.filter(isUpdate),
      pull.collect(cb)
    )
  }
}
