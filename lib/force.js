const { promisify: p } = require('util')

// Look at you reading the source code <3
// You've found a function NOT documented NOR in the the README
//
// This is because it's dangerous and might provide bad UX (time will tell)
//
// These functions will force-resolve any conflicting fields in order to merge
// a branched state. It just chooses a "winner" based on the branch most
// recently edited

module.exports = function Force (crut) {
  function forceUpdate (id, details, cb) {
    if (cb === undefined) return p(forceUpdate)(id, details)

    crut.update(id, details, (err, updateId) => {
      if (err === null) return cb(null, updateId)
      if (!err.conflictFields) return cb(err)

      crut.read(id, (err, record) => {
        if (err) return cb(err)

        for (const field of record.conflictFields) {
          details[field] = record.states[0][field]
          // we grab the first state because states are ordered by update time
        }
        // NOTE this will only work with the @tangle/overwrite fields
        // but so far these are the only ones which conflict!

        crut.update(id, details, cb)
      })
    })
  }

  function forceTombstone (id, details, cb) {
    if (cb === undefined) return p(forceTombstone)(id, details)

    crut.tombstone(id, details, (err, updateId) => {
      if (err === null) return cb(null, updateId)
      if (!err.conflictFields) return cb(err)

      forceUpdate(id, {}, err => {
        if (err) return cb(err)
        crut.tombstone(id, details, cb)
      })
    })
  }

  return {
    forceUpdate,
    forceTombstone
  }
}
