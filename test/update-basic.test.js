const test = require('tape')
const pull = require('pull-stream')
const keys = require('ssb-keys')

const { SSB, Spec, replicate } = require('./helpers')
const CRUT = require('../')

test('update', t => {
  const ssb = SSB()
  const spec = Spec()
  const crut = new CRUT(ssb, spec)

  const Māui = ssb.id
  crut.create(
    {
      parent: 'Taranga',
      preferredName: 'Māui',
      attendees: {
        add: [{ id: Māui, seq: 33 }]
      }
    },
    (err, profileId) => {
      t.error(err, 'creates a profile')

      crut.update(
        profileId,
        {
          attendees: {
            add: [{ id: '@Hine-nui-te-pō', seq: 5000 }],
            remove: [{ id: Māui, seq: 41 }]
          }
        },
        (err, updateId) => {
          t.error(err, 'publish an update with crut.update')

          crut.read(profileId, (err, profile) => {
            if (err) throw err

            ssb.get(updateId, (_, value) => {
              const expected = {
                key: profileId,
                type: spec.type,
                originalAuthor: ssb.id,
                parent: 'Taranga',
                child: null,
                recps: null,
                preferredName: 'Māui',
                legalName: null,
                attendees: {
                  [Māui]: [{ start: 33, end: 41 }],
                  '@Hine-nui-te-pō': [{ start: 5000, end: null }]
                },
                tombstone: null,
                states: [],
                conflictFields: []
              }

              t.deepEqual(profile, expected, 'read returns updated state')

              ssb.close()
              t.end()
            })
          })
        }
      )
    }
  )
})

test('update - promise', async t => {
  const ssb = SSB()
  const spec = Spec()
  const crut = new CRUT(ssb, spec)

  const Māui = ssb.id
  const profileId = await crut.create(
    {
      parent: 'Taranga',
      preferredName: 'Māui',
      attendees: {
        add: [{ id: Māui, seq: 33 }]
      }
    }
  )
  const updateId = await crut.update(
    profileId,
    {
      attendees: {
        add: [{ id: '@Hine-nui-te-pō', seq: 5000 }],
        remove: [{ id: Māui, seq: 41 }]
      }
    }
  )
  const profile = await crut.read(profileId)
  ssb.get(updateId, (_, value) => {
    const expected = {
      key: profileId,
      type: spec.type,
      originalAuthor: ssb.id,
      parent: 'Taranga',
      child: null,
      recps: null,
      preferredName: 'Māui',
      legalName: null,
      attendees: {
        [Māui]: [{ start: 33, end: 41 }],
        '@Hine-nui-te-pō': [{ start: 5000, end: null }]
      },
      tombstone: null,
      states: [],
      conflictFields: []
    }
    t.deepEqual(profile, expected, 'read returns updated state')
    ssb.close()
    t.end()
  })
})

test('update (invalid - fails isValidNextStep)', t => {
  const me = SSB()
  const friend = SSB()
  const spec = Spec()

  const meCrut = new CRUT(me, spec)
  const friendCrut = new CRUT(friend, spec)

  meCrut.create(
    {
      parent: 'Taranga',
      attendees: {
        add: [{ id: me.id, seq: 1 }]
      }
    },
    (err, profileId) => {
      t.error(err, 'create a profile')

      replicate({ from: me, to: friend }, () => {
        friendCrut.update(
          profileId,
          {
            attendees: {
              add: [{ id: friend.id, seq: 1 }]
            }
          },
          (err) => {
            t.match(err.message, /Invalid update/)

            me.close()
            friend.close()
            t.end()
          }
        )
      })
    }
  )
})

test('update (ssb-recps-guard)', t => {
  const ssb = SSB({ recpsGuard: true })
  const spec = Spec()
  const crut = new CRUT(ssb, spec)

  crut.create({
    parent: 'mix',
    attendees: { add: [{ id: ssb.id, seq: 1 }] },
    allowPublic: true
  }, (err, id) => {
    t.error(err, 'create a public record')

    crut.update(id, { preferredName: 'M' }, (err) => {
      t.match(err.message, /recps-guard|tribes.publish requires content.recps/, 'recps-guard blocks public updates')

      crut.update(id, { preferredName: 'M', allowPublic: true }, (err) => {
        t.error(err, 'allows public update if allowPublic: true')
        ssb.close()
        t.end()
      })
    })
  })
})

test('update (copy root recps)', t => {
  const ssb = SSB({ tribes: true })
  const spec = Spec()
  const crut = new CRUT(ssb, spec)

  crut.create({
    parent: 'mix',
    attendees: { add: [{ id: ssb.id, seq: 1 }] },
    recps: [ssb.id]
  }, (err, rootId) => {
    t.error(err, 'creates a private record')

    crut.update(rootId, { preferredName: 'M' }, (err, updateId) => {
      t.error(err, 'published update')

      ssb.get(updateId, (_, value) => {
        // doesn't work like this in db2, get is not exposed over the network
        if (!ssb.db) { t.equal(typeof value.content, 'string', 'content was auto encrypted') }
        ssb.close()
        t.end()
      })
    })
  })
})

test('update (getTransformation)', t => {
  const ssb = SSB()
  const ssb2 = SSB()

  const spec = Spec({
    getTransformation: m => {
      const { author, content } = m.value

      const _content = { ...content } // shallow clone!
      if (_content.preferredName) {
        _content.preferredName = {
          set: { author, value: _content.preferredName.set }
        }
      }

      return _content
    }
  })
  const crut = new CRUT(ssb, spec)
  const crut2 = new CRUT(ssb2, spec)

  const Māui = ssb.id
  crut.create(
    {
      parent: 'Taranga',
      preferredName: 'Māui',
      legalName: 'Ben',
      attendees: {
        add: [
          { id: Māui, seq: 33 },
          { id: ssb2.id, seq: 1 }
        ]
      }
    },
    (err, profileId) => {
      t.error(err, 'creates a profile')

      replicate({ from: ssb, to: ssb2 }, (err) => {
        if (err) throw err
        crut2.update(
          profileId,
          { preferredName: 'Māui !!!' },
          (err, updateId) => {
            t.error(err, 'someone else updates')

            crut2.read(profileId, (err, profile) => {
              if (err) throw err

              t.deepEqual(
                profile,
                {
                  key: profileId,
                  type: spec.type,
                  originalAuthor: ssb.id,
                  parent: 'Taranga',
                  child: null,
                  recps: null,
                  preferredName: { author: ssb2.id, value: 'Māui !!!' },
                  legalName: 'Ben',
                  attendees: {
                    [Māui]: [{ start: 33, end: null }],
                    [ssb2.id]: [{ start: 1, end: null }]
                  },
                  tombstone: null,
                  states: [],
                  conflictFields: []
                },
                'reads decorated info'
              )

              ssb2.get({ id: updateId, meta: true, private: true }, (err, m) => {
                if (err) throw err
                t.equal(m.value.content.preferredName.set, 'Māui !!!', 'saves simple info')

                ssb.close()
                ssb2.close()
                t.end()
              })
            })
          }
        )
      })
    }
  )
})

test('update (malformed attrs)', t => {
  const ssb = SSB()
  const spec = Spec()
  const crut = new CRUT(ssb, spec)

  const tests = [
    {
      input: 'dogo',
      expectedErr: 'input must be an Object'
    },
    // {
    //   input: ['dogo'],
    //   expectedErr: 'input must be an Object'
    // },
    {
      input: { dogo: 'pup' },
      expectedErr: 'unallowed inputs: dogo'
    },
    {
      input: null,
      expectedErr: 'input must be an Object'
    },
    {
      input: undefined,
      expectedErr: 'input must be an Object'
    },
    {
      input: 0,
      expectedErr: 'input must be an Object'
    }
  ]

  crut.create({ parent: 'dave' }, (err, recordId) => {
    if (err) throw err

    pull(
      pull.values(tests),
      pull.asyncMap(({ input, expectedErr }, cb) => {
        crut.create(input, (err) => {
          if (!err) {
            return cb(new Error(`Expected to see error '${expectedErr}'`))
          }

          t.match(err.message, new RegExp(expectedErr, 'g'), expectedErr)
          cb(null, null)
        })
      }),
      pull.collect((err) => {
        if (err) throw err

        ssb.close()
        t.end()
      })
    )
  })
})

test('update, opts = { feedId, create }', t => {
  const otherKeys = keys.generate()

  const ssb = SSB()
  const spec = Spec()
  const crut = new CRUT(ssb, spec, {
    feedId: otherKeys.id,
    create (input, cb) {
      process.env.DB2
        ? ssb.db.create({ ...input, keys: otherKeys }, cb)
        : ssb.publish(input.content, cb)
    }
  })

  const input = {
    parent: 'mix',
    attendees: {
      add: [{ id: otherKeys.id, seq: 33 }]
    }
  }
  crut.create(input, (err, msgKey) => {
    t.error(err, 'creates')
    crut.update(msgKey, { preferredName: 'arj' }, (err, updateId) => {
      t.error(err, 'publish an update with crut.update')

      ssb.get(updateId, (err, value) => {
        if (err) t.error(err)
        if (process.env.DB2) t.equal(value.author, otherKeys.id, 'update with proper author')

        ssb.close()
        t.end()
      })
    })
  })
})

test('update, isValidNextStep', t => {
  const ssb = SSB()
  const spec = Spec({
    isValidNextStep (ctx, node, ssb) {
      t.deepEqual(ctx.tips.length, 1, 'ctx is set')
      t.deepEqual(node.author, ssb.id, 'node is set')
      t.deepEqual(ssb, ssb, 'ssb is available in isValidNextStep')
      return true
    }
  })
  const crut = new CRUT(ssb, spec)

  const input = {
    parent: 'mix',
    attendees: {
      add: [{ id: ssb.id, seq: 33 }]
    }
  }
  crut.create(input, (err, msgKey) => {
    t.error(err, 'creates')

    ssb.close()
    t.end()
  })
})
