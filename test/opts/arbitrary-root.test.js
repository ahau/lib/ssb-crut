const test = require('tape')
const { promisify: p } = require('util')
const Overwrite = require('@tangle/overwrite')

const { SSB, fixRecps } = require('../helpers')
const CRUT = require('../..')

test('opts.arbitraryRoot', async t => {
  const ssb = SSB({ tribes: true })
  const spec = {
    type: 'settings',
    props: {
      autoFollow: Overwrite({ valueSchema: { type: 'boolean' } })
    },
    arbitraryRoot: true
  }
  const settings = new CRUT(ssb, spec)

  /* calling #create */
  const err = await settings.create({ autoFollow: true }).catch(err => err)
  t.match(err.message, /cannot call create on a crut with spec.arbitraryRoot/, 'cannot call #create')

  /* public root */
  let root = ssb.db ? await p(ssb.db.create)({ content: { type: 'profile' } }) : await p(ssb.publish)({ type: 'profile' })
  let updateId = await settings.update(root.key, { autoFollow: true })
  let update = await p(ssb.get)({ id: updateId, private: true })
  t.deepEqual(
    update.content,
    fixRecps({
      type: 'settings',
      autoFollow: { set: true },
      tangles: {
        settings: {
          root: root.key, previous: [root.key]
        }
      }
    }, ssb),
    'public #update'
  )
  let result = await settings.read(root.key)
  t.deepEqual(
    result,
    {
      key: root.key,
      originalAuthor: ssb.id,
      type: 'settings',
      recps: null,
      states: [],
      autoFollow: true,
      tombstone: null,
      conflictFields: []
    },
    'public #read'
  )
  // TODO write test with multiple updates?

  /* private root */
  root = ssb.db ? await p(ssb.db.create)({ content: { type: 'profile', recps: [ssb.id] }, encryptionFormat: 'box2' }) : await p(ssb.publish)({ type: 'profile', recps: [ssb.id] })
  updateId = await settings.update(root.key, { autoFollow: true })
  update = await p(ssb.get)({ id: updateId, private: true })

  t.deepEqual(
    update.content,
    {
      type: 'settings',
      autoFollow: { set: true },
      tangles: {
        settings: {
          root: root.key, previous: [root.key]
        }
      },
      recps: [ssb.id]
    },
    'private #update'
  )

  result = await settings.read(root.key)
  t.deepEqual(
    result,
    {
      key: root.key,
      originalAuthor: ssb.id,
      type: 'settings',
      recps: [ssb.id],
      states: [],
      autoFollow: true,
      tombstone: null,
      conflictFields: []
    },
    'private #read'
  )

  // HACK - we need the last posted message to the group for the
  // groups tangle (it's a group/add-member adding ourselves)
  let lastMsgId
  ssb.post(m => (lastMsgId = m.key))

  /* private group root */
  const { groupId, groupInitMsg } = await p(ssb.tribes.create)({})
  root = groupInitMsg.key

  const previous = lastMsgId

  updateId = await settings.updateGroup(groupId, { autoFollow: true })
  update = await p(ssb.get)({ id: updateId, private: true })

  t.deepEqual(
    update.content,
    {
      type: 'settings',
      autoFollow: { set: true },
      tangles: {
        settings: { root, previous: [root] },
        group: { root, previous: [previous] }
      },
      recps: [groupId]
    },
    'private-group #updateGroup'
  )

  result = await settings.readGroup(groupId)
  t.deepEqual(
    result,
    {
      key: root,
      originalAuthor: ssb.id,
      type: 'settings',
      recps: [groupId],
      states: [],
      autoFollow: true,
      tombstone: null,
      conflictFields: []
    },
    'private #readGroup'
  )

  await settings.tombstoneGroup(groupId, {})

  ssb.close()
  t.end()
})

test('opts.arbitraryRoot (false, then use *Group methods)', async t => {
  const ssb = SSB({ tribes: true })

  const spec = {
    type: 'settings',
    props: {
      autoFollow: Overwrite({ valueSchema: { type: 'boolean' } })
    },
    arbitraryRoot: false // << NOTE difference!
  }
  const settings = new CRUT(ssb, spec)

  const { groupId } = await p(ssb.tribes.create)({})

  const err = await settings.updateGroup(groupId, { autoFollow: true })
    .catch(err => err)

  t.match(err.message, /cannot use updateGroup with spec.arbitraryRoot = false/, 'useful error')

  ssb.close()
  t.end()
})
