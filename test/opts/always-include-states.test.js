const test = require('tape')
const { promisify: p } = require('util')
const Overwrite = require('@tangle/overwrite')

const { SSB } = require('../helpers')
const CRUT = require('../..')

test('opts.alwaysIncludeStates', async t => {
  const ssb = SSB()
  const spec = {
    type: 'event',
    props: {
      title: Overwrite()
    }
  }
  const opts = {
    alwaysIncludeStates: true
  }
  const event = new CRUT(ssb, spec, opts)

  /* calling #create */
  const id = await event.create({ title: 'somnophore' })
  const record = await event.read(id)

  // console.log(record)
  t.equal(record.states.length, 1, 'includes states')

  p(ssb.close)(true)
  t.end()
})
