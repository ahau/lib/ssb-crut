const test = require('tape')
const Overwrite = require('@tangle/overwrite')
const LinearAppend = require('@tangle/linear-append')

const { SSB, Spec, fixRecps } = require('../helpers')
const CRUT = require('../..')

// getTransformation is tested in the basic CRUT methods, this is an advanced test
test('opts.getTransformation', t => {
  const ssb = SSB()

  // WARNING
  // the following pattern could be a terrible idea, it's just an experiment which demonstrates what's possbile
  // consider carefully before using in production
  const spec = Spec({
    props: {
      preferredName: Overwrite(),
      history: LinearAppend({ // a dummy field we use to populate from other fields
        keyPattern: '^\\d+$',
        valueSchema: {
          type: 'object',
          properties: {
            field: { type: 'string' },
            author: { type: 'string', typePattern: '^@.+ed25519$' },
            value: { type: 'string' }
          },
          required: ['field', 'author', 'value']
        }
      })
    },
    staticProps: {},
    nextStepData: {},
    getTransformation (m) {
      const T = { ...m.value.content } // must not mutate the content, otherwise this gets published

      // populate the history field as we go!
      if (T.preferredName && T.preferredName.set) {
        T.history = Object.assign(T.history || {}, {
          [m.value.timestamp]: { field: 'preferredName', author: m.value.author, value: T.preferredName.set }
          // ['a'+m.value.timestamp]: { field: 'preferredName', author: m.value.author, value: T.preferredName.set }
          // enable this value (and disable one above) to see test fail
        })
      }

      return T
    }
  })
  const crut = new CRUT(ssb, spec)

  crut.create(
    { preferredName: 'huatli' },
    (err, profileId) => {
      if (err) throw err

      ssb.get(profileId, (err, value) => {
        if (err) throw err

        t.deepEqual(
          value.content,
          fixRecps({
            type: 'profile/person',
            preferredName: { set: 'huatli' },
            tangles: {
              profile: { root: null, previous: null }
            }
          }, ssb),
          'stores correctly in db'
        )

        crut.read(profileId, (err, profile) => {
          if (err) throw err

          t.deepEqual(
            profile.history,
            [
              { field: 'preferredName', author: ssb.id, value: 'huatli' }
            ],
            'reading can be mutated by spec.getTransformation'
          )
          ssb.close()
          t.end()
        })
      })
    }
  )
})

test('opts.getTransformation (prunes some field!)', t => {
  const ssb = SSB()

  const spec = {
    type: 'profile/person',
    props: {
      preferredName: Overwrite(),
      legalName: Overwrite()
    },
    staticProps: {},
    nextStepData: {},
    getTransformation (m) { // << CUSTOM
      const T = {}
      const {
        preferredName
        // legalName // old field we want to ignore
      } = m.value.content
      if (preferredName) T.preferredName = preferredName

      return T
    }
  }
  const crut = new CRUT(ssb, spec)

  crut.create({ preferredName: 'huatli', legalName: 'hp' }, (err, profileId) => {
    t.error(err, 'crut.create')

    ssb.get(profileId, (err, value) => {
      if (err) throw err

      t.deepEqual(
        value.content,
        fixRecps({
          type: 'profile/person',
          preferredName: { set: 'huatli' },
          legalName: { set: 'hp' },
          tangles: {
            profile: { root: null, previous: null }
          }
        }, ssb),
        'persists correct root message'
      )

      crut.update(profileId, { preferredName: 'HUATLI', legalName: 'HP' }, (err, updateId) => {
        t.error(err, 'crut.update')

        ssb.get(updateId, (err, value) => {
          if (err) throw err

          t.deepEqual(
            value.content,
            fixRecps({
              type: 'profile/person',
              preferredName: { set: 'HUATLI' },
              legalName: { set: 'HP' },
              tangles: {
                profile: { root: profileId, previous: [profileId] }
              }
            }, ssb),
            'persists correct update message'
          )

          crut.read(profileId, (err, profile) => {
            if (err) throw err

            t.deepEqual(
              profile.preferredName,
              'HUATLI',
              'crut.read processes messages mutated by spec.getTransformation'
            )
            ssb.close()
            t.end()
          })
        })
      })
    })
  })
})

test('opts.getTransformation (content mutated = UNHAPPY)', t => {
  const ssb = SSB()

  const spec = Spec({
    getTransformation: m => {
      const { author, content } = m.value

      if (content.preferredName) {
        content.preferredName = {
          set: { author, value: content.preferredName.set }
        }
      }

      return content
    }
  })
  const crut = new CRUT(ssb, spec)

  const Māui = ssb.id
  crut.create(
    {
      parent: 'Taranga',
      preferredName: 'Māui',
      legalName: 'Ben',
      attendees: {
        add: [
          { id: Māui, seq: 33 }
        ]
      }
    },
    (err) => {
      t.match((err && err.message) || '', /getTransformation mutated content/, 'does not publish mutated content')

      ssb.close()
      t.end()
    }
  )
})

test('opts.getTransformation (content outside of schema = UNHAPPY)', t => {
  const ssb = SSB()

  const spec = Spec({
    props: {
      preferredName: Overwrite()
    },
    staticProps: {},
    nextStepData: {},
    getTransformation (m) {
      const T = { ...m.value.content } // must not mutate the content, otherwise this gets published

      // populate the history field as we go!
      // BUT this time the history field is not a valid props!
      if (T.preferredName && T.preferredName.set) {
        T.history = Object.assign(T.history || {}, {
          [m.value.timestamp]: { field: 'preferredName', author: m.value.author, value: T.preferredName.set }
          // ['a'+m.value.timestamp]: { field: 'preferredName', author: m.value.author, value: T.preferredName.set }
          // enable this value (and disable one above) to see test fail
        })
      }

      return T
    }
  })
  const crut = new CRUT(ssb, spec)

  crut.create(
    { preferredName: 'huatli' },
    (err, profileId) => {
      t.match(err.message, /data has additional properties/, 'message is not published')

      ssb.close()
      t.end()
    }
  )
})
