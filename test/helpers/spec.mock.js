const overwrite = require('@tangle/overwrite')()
const complexSet = require('@tangle/complex-set')()

module.exports = function MockSpec (obj = {}) {
  const base = {
    type: 'profile/person',
    props: {
      preferredName: overwrite,
      legalName: overwrite,

      attendees: complexSet
    },

    // optional
    // typePattern: '^link\\/\\*$'
    tangle: 'profile',
    isValidNextStep,
    hooks: {
      isRoot: [
        (content) => true
      ]
      // isUpdate: []
    },
    staticProps: {
      parent: { type: 'string', required: true },
      child: { type: 'string' }
      // recps: { type: 'custom', schema: {} }
      // NOTE we're only using the keys here at the moment
      // In future would like to make it so we can build root schema from this
    },
    nextStepData: {
    }
  }

  return Object.assign({}, base, obj)
}

function isValidNextStep ({ root, tips, graph }, m) {
  if (root === undefined) throw new Error('plz can has root') // needed for ssb-crut-authors

  isValidNextStep.error = null
  if (m.previous === null) return true

  const isValid = tips.every(tip => {
    return Object.keys(tip.T.attendees)
      .some(attendee => attendee === m.author)
      // NOTE this only checks if a given author was ever mentioned
      // in the attendees transformations, not if they were added / removed!
  })
  if (isValid) return true
  else {
    isValidNextStep.error = new Error(`Invalid update - ${m.author} is not attendee, so can't update`)
  }
}
