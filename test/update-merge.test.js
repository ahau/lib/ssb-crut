const test = require('tape')

const { SSB, setupForked } = require('./helpers')

test('update-merge (update a forked state)', t => {
  const ssb = SSB()
  const Māui = ssb.id

  setupForked(
    ssb,
    {
      parent: 'Taranga',
      preferredName: 'Māui',
      attendees: {
        add: [{ id: Māui, seq: 33 }]
      }
    },
    [
      { preferredName: 'Māui A' },
      { preferredName: 'Māui B' }
    ],
    (err, { spec, crut, rootId, forkKeys }) => {
      if (err) throw err
      /* now see what crut.update does */
      crut.update(rootId, { preferredName: 'Māui C' }, (err, updateId) => {
        t.error(err, 'use crut.update on forked state')

        crut.read(rootId, (err, profile) => {
          if (err) throw err

          const expected = {
            key: rootId,
            type: spec.type,
            originalAuthor: Māui,
            parent: 'Taranga',
            child: null,
            recps: null,
            preferredName: 'Māui C',
            legalName: null,
            attendees: {
              [Māui]: [{ start: 33, end: null }]
            },
            tombstone: null,
            states: [],
            conflictFields: []
          }
          t.deepEqual(
            profile,
            expected,
            'merges the current states (overwriting the preferredName)'
          )

          ssb.close()
          t.end()
        })
      })
    })
})

test('update-merge (fail to update forked state)', t => {
  const ssb = SSB()
  const Māui = ssb.id

  setupForked(
    ssb,
    {
      parent: 'Taranga',
      preferredName: 'Māui',
      attendees: {
        add: [{ id: Māui, seq: 33 }]
      }
    },
    [
      { preferredName: 'Māui A' },
      { preferredName: 'Māui B' }
    ],
    (err, { spec, crut, rootId, forkKeys }) => {
      if (err) throw err

      crut.update(rootId, { attendees: { remove: [{ id: Māui, seq: 34 }] } }, (err, updateId) => {
        if (!err) t.fail('expected error on update')
        else {
          t.match(err.message, /Merge conflict on fields \[preferredName\]/, 'invalid merge update on forked state')
          t.deepEqual(err.conflictFields, ['preferredName'], 'err.conflictFields provides an array of fields')
        }

        crut.read(rootId, (err, profile) => {
          if (err) throw err

          const states = [
            {
              key: forkKeys[1],
              preferredName: 'Māui B',
              legalName: null,
              attendees: {
                [Māui]: [{ start: 33, end: null }]
              },
              tombstone: null
            },
            {
              key: forkKeys[0],
              preferredName: 'Māui A',
              legalName: null,
              attendees: {
                [Māui]: [{ start: 33, end: null }]
              },
              tombstone: null
            }
          ]

          const expected = {
            key: rootId,
            type: spec.type,
            originalAuthor: ssb.id,
            parent: 'Taranga',
            child: null,
            recps: null,
            preferredName: 'Māui B',
            legalName: null,
            attendees: {
              [Māui]: [{ start: 33, end: null }]
            },
            tombstone: null,
            states,
            conflictFields: ['preferredName']
          }
          t.deepEqual(
            profile,
            expected,
            'Tips have not been extended by failed merge update'
          )

          ssb.close()
          t.end()
        })
      })
    })
})

test('update-merge (two fields conflicting that are unresolved by update)', t => {
  const ssb = SSB()
  const Māui = ssb.id

  setupForked(
    ssb,
    {
      parent: 'Taranga',
      preferredName: 'Māui',
      attendees: {
        add: [{ id: Māui, seq: 33 }]
      }
    },
    [
      { preferredName: 'Māui A', legalName: 'Legal A' },
      { preferredName: 'Māui B', legalName: 'Legal B' }
    ],
    (err, { spec, crut, rootId, forkKeys }) => {
      if (err) throw err

      crut.update(rootId, { attendees: { remove: [{ id: Māui, seq: 34 }] } }, (err, updateId) => {
        if (!err) t.fail('expected error on update')
        else {
          t.match(err.message, /Merge conflict on fields \[preferredName,legalName\]/, 'invalid merge update on forked state')
          t.deepEqual(err.conflictFields, ['preferredName', 'legalName'], 'err.conflictFields provides an array of fields')
        }

        crut.read(rootId, (err, profile) => {
          if (err) throw err

          const states = [
            {
              key: forkKeys[1],
              preferredName: 'Māui B',
              legalName: 'Legal B',
              attendees: {
                [Māui]: [{ start: 33, end: null }]
              },
              tombstone: null
            },
            {
              key: forkKeys[0],
              preferredName: 'Māui A',
              legalName: 'Legal A',
              attendees: {
                [Māui]: [{ start: 33, end: null }]
              },
              tombstone: null
            }
          ]

          const expected = {
            key: rootId,
            type: spec.type,
            originalAuthor: ssb.id,
            parent: 'Taranga',
            child: null,
            recps: null,
            preferredName: 'Māui B',
            legalName: 'Legal B',
            attendees: {
              [Māui]: [{ start: 33, end: null }]
            },
            tombstone: null,
            states,
            conflictFields: ['preferredName', 'legalName']
          }
          t.deepEqual(
            profile,
            expected,
            'Tips have not been extended by failed merge update'
          )

          ssb.close()
          t.end()
        })
      })
    })
})

test('update-merge (two updates on merged state)', t => {
  const ssb = SSB()
  const Māui = ssb.id

  setupForked(
    ssb,
    {
      parent: 'Taranga',
      preferredName: 'Māui',
      attendees: {
        add: [{ id: Māui, seq: 33 }]
      }
    },
    [
      { preferredName: 'Māui A' },
      { preferredName: 'Māui B' }
    ],
    (err, { spec, crut, rootId, forkKeys }) => {
      if (err) throw err
      /* now see what crut.update does */
      crut.update(rootId, { preferredName: 'Māui C' }, (err, updateId) => {
        t.error(err, 'use crut.update on forked state')

        const manualUpdateD = {
          type: spec.type,
          legalName: { set: 'Māui D' },
          tangles: {
            [spec.tangle]: { root: rootId, previous: [forkKeys[1]] }
          }
        }
        // Manually extend update B with D
        /*
            R --> A --> C
            R --> B --> C
                  B --> D
            */

        const publish = (ssb, content, cb) => ssb.db ? ssb.db.create({ content }, cb) : ssb.publish(content, cb)

        publish(ssb, manualUpdateD, (err, updateD) => {
          t.error(err, 'manually publish a second forked update')

          // Expect that crut.update will merge C and D.
          crut.update(rootId, { preferredName: 'Māui E' }, (err, updateId) => {
            t.error(err, 'use crut.update on second forked state')

            crut.read(rootId, (err, profile) => {
              if (err) throw err

              const expected = {
                key: rootId,
                type: spec.type,
                originalAuthor: Māui,
                parent: 'Taranga',
                child: null,
                recps: null,
                preferredName: 'Māui E',
                legalName: 'Māui D',
                attendees: {
                  [Māui]: [{ start: 33, end: null }]
                },
                tombstone: null,
                states: [],
                conflictFields: []
              }
              t.deepEqual(
                profile,
                expected,
                'merges the current states (C and D merged into E)'
              )

              ssb.close()
              t.end()
            })
          })
        })
      })
    })
})

test('update-merge (branches with different complex-set for mapFromInput)', { objectPrintDepth: 6 }, t => {
  const ssb = SSB()
  const Māui = ssb.id

  setupForked(
    ssb,
    {
      parent: 'Taranga',
      preferredName: 'Māui',
      attendees: {
        add: [{ id: Māui, seq: 33 }]
      }
    },
    [
      {
        preferredName: 'Māui A',
        attendees: {
          remove: [{ id: Māui, seq: 39 }]
        }
      },
      {
        preferredName: 'Māui B',
        attendees: {
          remove: [{ id: Māui, seq: 34 }]
        }
      }
    ],
    (err, { spec, crut, rootId, forkKeys }) => {
      if (err) throw err

      /* now see what crut.update does */
      crut.update(rootId,
        {
          preferredName: 'Māui C',
          attendees: {
            add: [
              { id: Māui, seq: 34 }, // cancels the remove at seq 34 above!
              { id: Māui, seq: 200 },
              { id: Māui, seq: 404 } // redundent at this point
            ]
          }
        }, (err, updateId) => {
          t.error(err, 'use crut.update on forked state')

          crut.read(rootId, (err, profile) => {
            if (err) throw err

            const expected = {
              key: rootId,
              type: spec.type,
              originalAuthor: ssb.id,
              parent: 'Taranga',
              child: null,
              recps: null,
              preferredName: 'Māui C',
              legalName: null,
              attendees: {
                [Māui]: [{ start: 33, end: 39 }, { start: 200, end: null }]
              },
              tombstone: null,
              states: [],
              conflictFields: []
            }
            t.deepEqual(
              profile,
              expected,
              'merges the current states (complexSet involved)'
            )

            ssb.close()
            t.end()
          })
        })
    }
  )
})
